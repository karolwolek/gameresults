﻿using GamesResults.Models;
using Microsoft.EntityFrameworkCore;
using Npgsql;

namespace GamesResults.Models
{
    public class GameResultsContext : DbContext
    {
        public GameResultsContext(DbContextOptions<GameResultsContext> options) : base(options)
        {
        }


        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseNpgsql("Server=localhost;Database=GameResults;User Id=postgres;Password=123456;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Player>().ToTable("Player", "public");
            modelBuilder.Entity<Game>().ToTable("Game", "public");


            modelBuilder.Entity<PlayerResult>().ToTable("PlayerResult", "public");
            modelBuilder.Entity<PlayerResult>()
                .HasOne(m => m.Gameplay)
                .WithMany(m => m.Results)
                .HasForeignKey(m => m.GameplayID)
                .IsRequired();
            modelBuilder.Entity<PlayerResult>()
                .HasOne(m => m.Player)
                .WithMany(m => m.Results)
                .HasForeignKey(m => m.PlayerID)
                .IsRequired();

            modelBuilder.Entity<Gameplay>().ToTable("Gameplay", "public");
            modelBuilder.Entity<Gameplay>()
                .HasOne(m => m.Game)
                .WithMany(m => m.Gameplays)
                .HasForeignKey(m => m.GameID)
                .IsRequired(); ;


        }

        public DbSet<Player> Players { get; set; }
        public DbSet<PlayerResult> PlayerResults { get; set; }
        public DbSet<Game> Games { get; set; }
        public DbSet<Gameplay> Gameplays { get; set; }
    }
}