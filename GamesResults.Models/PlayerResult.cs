﻿using System;

namespace GamesResults.Models
{
    public class PlayerResult
    {
        public long ID { get; set; }
        public long PlayerID { get; set; }
        public long GameplayID { get; set; }
        public long Result { get; set; }
        public bool IsWinner { get; set; }

        public virtual Player Player { get; set; }
        public virtual Gameplay Gameplay { get; set; }

    }
}
