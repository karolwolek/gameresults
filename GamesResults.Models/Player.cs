﻿using System;
using System.Collections.Generic;

namespace GamesResults.Models
{
    public class Player
    {
        public long ID { get; set; }
        public string Name { get; set; }

        public List<PlayerResult> Results { get; set; }
    }
}
