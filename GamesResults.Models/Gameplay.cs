﻿using System;
using System.Collections.Generic;

namespace GamesResults.Models
{
    public class Gameplay
    {
        public long ID { get; set; }
        public long GameID { get; set; }
        public DateTime Date { get; set; }

        public virtual Game Game { get; set; }
        public List<PlayerResult> Results { get; set; }
    }
}
