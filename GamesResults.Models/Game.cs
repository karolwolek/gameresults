﻿using System;
using System.Collections.Generic;

namespace GamesResults.Models
{
    public class Game
    {
        public long ID { get; set; }
        public string Name { get; set; }
        public int MinPlayers { get; set; }
        public int? MaxPlayers { get; set; }

        public List<Gameplay> Gameplays { get; set; }
    }
}
