﻿using System;

namespace GamesResults.Enums
{
    public enum ObjectState
    {
        NotChanged,
        New,
        Modified,
        Deleted
    }
}
