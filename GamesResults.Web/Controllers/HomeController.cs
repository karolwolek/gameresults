﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using GamesResults.Web.Models;
using GamesResults.Web.ViewModels;
using Microsoft.Extensions.Caching.Memory;

namespace GamesResults.Web.Controllers
{
    public class HomeController : BaseController
    {

        public HomeController(IMemoryCache cache): base(cache)
        {

        }

        public IActionResult Index()
        {
            return View("~/Views/Home.cshtml");
        }
    }
}
