﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using GamesResults.Web.Models;
using GamesResults.Web.ViewModels;

namespace GamesResults.Web.Controllers
{
    public class GameplayController : BaseController
    {
        private GameplayModel gameplayModel;

        public GameplayController(IMemoryCache cache, GameplayModel _gameplayModel): base(cache)
        {
            gameplayModel = _gameplayModel;
        }

        public IActionResult Index()
        {
            var gameplayList = gameplayModel.GetGameplaysList();
            return View("~/Views/GameplayList.cshtml",gameplayList);
        }


        public IActionResult GetGameplayPreview(long id)
        {
            var viewModel = gameplayModel.PrepareGameplayViewModel(id);
            return PartialView("~/Views/GameplayForm.cshtml", viewModel);
        }


        public IActionResult AddNewGameplayForm()
        {
            var viewModel = gameplayModel.PrepareGameplayViewModel(null);
            return PartialView("~/Views/GameplayForm.cshtml", viewModel);
        }


        public IActionResult SaveGameplay(GameplayViewModel form)
        {
            gameplayModel.Validate(form, ModelState);

            if (!ModelState.IsValid)
            {
                return PartialView("~/Views/GamePlayForm.cshtml", form);
            }

            var result = gameplayModel.SaveForm(form);

            return Json(new { success = result });
        }
    }
}