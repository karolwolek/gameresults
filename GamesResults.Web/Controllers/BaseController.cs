﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;

namespace GamesResults.Web.Controllers
{
    public class BaseController : Controller
    {
        private IMemoryCache _cache;

        public BaseController(IMemoryCache cache)
        {
            _cache = cache;
        }
    }
}