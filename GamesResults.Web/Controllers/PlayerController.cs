﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using GamesResults.Web.Models;
using GamesResults.Web.ViewModels;

namespace GamesResults.Web.Controllers
{
    public class PlayerController : BaseController
    {
        private PlayerModel playerModel;

        public PlayerController(IMemoryCache cache, PlayerModel _playerModel): base(cache)
        {
            playerModel = _playerModel;
        }

        public IActionResult Index()
        {
            var playerList = playerModel.GetPlayersList();
            return View("~/Views/PlayersList.cshtml",playerList);
        }


        public IActionResult GetPlayerPreview(long id)
        {
            var viewModel = playerModel.PreparePlayerViewModel(id);
            return PartialView("~/Views/PlayerForm.cshtml", viewModel);
        }

        public IActionResult AddnewPlayerForm()
        {
            var viewModel = playerModel.PreparePlayerViewModel(null);
            return PartialView("~/Views/PlayerForm.cshtml", viewModel);
        }

        public IActionResult SavePlayer(PlayerViewModel form)
        {
            playerModel.Validate(form, ModelState);

            if (!ModelState.IsValid)
            {
                form.State = Enums.ObjectState.New;
                return PartialView("~/Views/PlayerForm.cshtml", form);
            }

            var result = playerModel.SaveForm(form);

            return Json(new { success = result });
        }

    }
}