﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using GamesResults.Web.Models;
using GamesResults.Web.ViewModels;

namespace GamesResults.Web.Controllers
{
    public class GameController : BaseController
    {

        private GameModel gameModel;

        public GameController(IMemoryCache cache, GameModel _gameModel): base(cache)
        {
            gameModel = _gameModel;
        }

        public IActionResult Index()
        {
            var gameList = gameModel.GetGamesList();
            return View("~/Views/GameList.cshtml",gameList);
        }

        public IActionResult GetGamePreview(long id)
        {
            var viewModel = gameModel.PrepareGameViewModel(id);
            return PartialView("~/Views/GameForm.cshtml",viewModel);
        }

        
        public IActionResult GetPlayersNumber(long id)
        {
            var viewModel = gameModel.PrepareGameViewModel(id);
            return Json(new { minPlayers = viewModel.MinPlayers, maxPlayers = viewModel.MaxPlayers });
        }


        public IActionResult AddNewGameForm(long id)
        {
            var viewModel = gameModel.PrepareGameViewModel(null);
            return PartialView("~/Views/GameForm.cshtml", viewModel);
        }


        public IActionResult SaveGame(GameViewModel form)
        {
            gameModel.Validate(form, ModelState);

            if (!ModelState.IsValid)
            {
                return PartialView("~/Views/PlayerForm.cshtml", form);
            }

            var result = gameModel.SaveForm(form);

            return Json(new { success = result });
        }
    }
}