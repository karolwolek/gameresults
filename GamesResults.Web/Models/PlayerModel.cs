﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GamesResults.DTO;
using GamesResults.Models;
using GamesResults.Services;
using GamesResults.Web.ViewModels;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace GamesResults.Web.Models
{

    public class PlayerModel
    {
        private PlayerService playerService;

        public PlayerModel(PlayerService _playerService)
        {
            playerService = _playerService;
        }

        public List<PlayerViewModel> GetPlayersList()
        {

            var serviceResposne = playerService.GetPlayersList();
            if (!serviceResposne.Success)
            {
                throw new Exception(serviceResposne.Message);
            }

            var players = serviceResposne.Response.Select(x => new PlayerViewModel(x)).ToList();

            return players;
        }


        public PlayerViewModel PreparePlayerViewModel(long? id)
        {
            var viewModel = new PlayerViewModel();
            if (id.HasValue)
            {
                var serviceResponse = playerService.GetPlayer(id.Value);
                if (!serviceResponse.Success)
                {
                    throw new Exception(serviceResponse.Message);
                }
                viewModel = new PlayerViewModel(serviceResponse.Response);
            }
            else
            {
                viewModel.State = Enums.ObjectState.New;
                viewModel.IsEdit = true;
            }

            return viewModel;
        }

        public void Validate(PlayerViewModel viewModel, ModelStateDictionary modelState)
        {
            if (string.IsNullOrWhiteSpace(viewModel.Name))
            {
                modelState.AddModelError("Name", "Nazwa gracza jest wymagana.");
            }
            var isUnique = true;
            if (!isUnique)
            {
                modelState.AddModelError("Name", "Gracz o takiej nazwie już istnieje");
            }
        }

        public bool SaveForm(PlayerViewModel form)
        {
            var model = new Player();
            model.Name = form.Name;

            var serviceResponse = playerService.SavePlayer(model);

            return serviceResponse.Success;
        }
    }

}
