﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GamesResults.DTO;
using GamesResults.Models;
using GamesResults.Services;
using GamesResults.Web.ViewModels;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace GamesResults.Web.Models
{
    public class GameModel
    {
        private GameService gameService;

        public GameModel(GameService _gameService)
        {
            gameService = _gameService;
        }

        public List<GameViewModel> GetGamesList()
        {

            var serviceResposne = gameService.GetGamesList();
            if (!serviceResposne.Success)
            {
                throw new Exception(serviceResposne.Message);
            }

            var games = serviceResposne.Response.Select(x => new GameViewModel(x)).ToList();

            return games;
        }

        public GameViewModel PrepareGameViewModel(long? id)
        {
            var viewModel = new GameViewModel();
            if (id.HasValue)
            {
                var serviceResponse = gameService.GetGame(id.Value);
                if (!serviceResponse.Success)
                {
                    throw new Exception(serviceResponse.Message);
                }
                viewModel = new GameViewModel(serviceResponse.Response);
            }
            else
            { 
                viewModel.State = Enums.ObjectState.New;
                viewModel.IsEdit = true;
            }
            

            return viewModel;
        }


        public void Validate(GameViewModel viewModel, ModelStateDictionary modelState)
        {
            if (string.IsNullOrWhiteSpace(viewModel.Name))
            {
                modelState.AddModelError("Name", "Nazwa gry jest wymagana.");
            }
            var isUnique = true;
            if (!isUnique)
            {
                modelState.AddModelError("Name", "Gra o takiej nazwie już istnieje");
            }
        }


        public bool SaveForm(GameViewModel form)
        {
            var model = new Game();
            model.Name = form.Name;
            model.MinPlayers = form.MinPlayers;
            model.MaxPlayers = form.MaxPlayers;

            var serviceResponse = gameService.SaveGame(model);

            return serviceResponse.Success;
        }
    }
}
