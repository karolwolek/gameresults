﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GamesResults.DTO;
using GamesResults.Models;
using GamesResults.Services;
using GamesResults.Web.ViewModels;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace GamesResults.Web.Models
{
    public class GameplayModel
    {
        private GameplayService gameplayService;
        private GameService gameService;

        public GameplayModel(GameplayService _gameplayService, GameService _gameService)
        {
            gameplayService = _gameplayService;
            gameService = _gameService;
        }

        public List<GameplayViewModel> GetGameplaysList()
        {

            var serviceResposne = gameplayService.GetGameplaysList();
            if (!serviceResposne.Success)
            {
                throw new Exception(serviceResposne.Message);
            }

            var gameplays = serviceResposne.Response.Select(x => new GameplayViewModel(x)).ToList();

            return gameplays;
        }

        public GameplayViewModel PrepareGameplayViewModel(long? id)
        {
            var viewModel = new GameplayViewModel();
            if (id.HasValue)
            {
                var serviceResponse = gameplayService.GetGameplay(id.Value);
                if (!serviceResponse.Success)
                {
                    throw new Exception(serviceResponse.Message);
                }
                viewModel = new GameplayViewModel(serviceResponse.Response);
            }
            else
            {
                viewModel.State = Enums.ObjectState.New;
                viewModel.IsEdit = true;
            }

            var additionalServiceResponse = gameService.GetGamesList();
            if (!additionalServiceResponse.Success)
            {
                throw new Exception(additionalServiceResponse.Message);
            }

            viewModel.GamesSelectList = new List<SelectListItem>() { new SelectListItem() {Value = "-1", Text = "" } };
            viewModel.GamesSelectList.AddRange(additionalServiceResponse.Response.Select(x => new SelectListItem() { Value = x.ID.ToString(), Text = x.Name }).ToList());

            return viewModel;
        }


        public void Validate(GameplayViewModel viewModel, ModelStateDictionary modelState)
        {

        }


        public bool SaveForm(GameplayViewModel form)
        {
            var model = new Gameplay();
            model.Date = form.Date;
            model.GameID = form.GameID;

            var serviceResponse = gameplayService.SaveGameplay(model);

            return serviceResponse.Success;
        }
    }
}
