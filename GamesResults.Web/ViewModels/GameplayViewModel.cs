﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GamesResults.DTO;
using GamesResults.Enums;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.ComponentModel.DataAnnotations;

namespace GamesResults.Web.ViewModels
{
    public class GameplayViewModel
    {
        public long ID { get; set; }
        public DateTime Date { get; set; }
        public long GameID { get; set; }
        public string GameName { get; set; }
        public bool IsEdit { get; set; }
        public ObjectState State { get; set; }
        public List<SelectListItem> GamesSelectList { get; set; }
        public List<ResultViewModel> PlayersResults { get; set; }

        public GameplayViewModel()
        {
            GamesSelectList = new List<SelectListItem>();
            PlayersResults = new List<ResultViewModel>();
        }

        public GameplayViewModel(GameplayDTO dto): this()
        {
            ID = dto.ID;
            GameID = dto.GameID;
            Date = dto.Date;
            GameName = dto.GameName;
            PlayersResults = dto.PlayersResults?.Select(x => new ResultViewModel(x)).ToList();
        }
    }
}
