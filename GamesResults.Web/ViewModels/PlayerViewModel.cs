﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GamesResults.DTO;
using GamesResults.Enums;

namespace GamesResults.Web.ViewModels
{
    public class PlayerViewModel
    {
        public long ID { get; set; }
        public string Name { get; set; }
        public bool IsEdit { get; set; }
        public ObjectState State { get; set; }

        public PlayerViewModel()
        {

        }

        public PlayerViewModel(PlayerDTO dto) : this()
        {
            ID = dto.ID;
            Name = dto.Name;
        }
    }
}
