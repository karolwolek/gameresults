﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GamesResults.DTO;
using GamesResults.Enums;

namespace GamesResults.Web.ViewModels
{
    public class GameViewModel
    {
        public long ID { get; set; }
        public string Name { get; set; }
        public int MinPlayers { get; set; }
        public int? MaxPlayers { get; set; }
        public bool IsEdit { get; set; }
        public ObjectState State { get; set; }

        public GameViewModel()
        {

        }

        public GameViewModel(GameDTO dto)
        {
            ID = dto.ID;
            Name = dto.Name;
            MinPlayers = dto.MinPlayers;
            MaxPlayers = dto.MaxPlayers;
        }
    }
}
