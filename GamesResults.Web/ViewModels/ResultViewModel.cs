﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GamesResults.Enums;
using GamesResults.DTO;

namespace GamesResults.Web.ViewModels
{
    public class ResultViewModel
    {
        public string PlayerName { get; set; }
        public long PlayerID { get; set; }
        public long Result { get; set; }
        public bool IsWinner { get; set; }
        public ObjectState State { get; set; }

        public ResultViewModel()
        {

        }

        public ResultViewModel(PlayerResultDTO dto)
        {
            PlayerID = dto.PlayerID;
            Result = dto.Result;
            IsWinner = dto.IsWinner;
        }
    }
}
