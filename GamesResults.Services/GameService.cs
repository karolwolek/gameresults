﻿using System;
using System.Linq;
using System.Collections.Generic;
using GamesResults.DTO;
using GamesResults.Models;

namespace GamesResults.Services
{
    public class GameService
    {
        private GameResultsContext context { get; }

        public GameService(GameResultsContext _context)
        {
            context = _context;
        }


        public BaseServiceResponse<List<GameDTO>> GetGamesList()
        {
            try
            {
                var response = context.Games.Select(x => new GameDTO()
                {
                    ID = x.ID,
                    Name = x.Name,
                    MinPlayers = x.MinPlayers,
                    MaxPlayers = x.MaxPlayers
                }
                );
                return new BaseServiceResponse<List<GameDTO>>(true) { Response = response.ToList() };
            }
            catch (Exception ex)
            {
                return new BaseServiceResponse<List<GameDTO>>(false) { Message = ex.ToString() };
            }
        }

        public BaseServiceResponse<GameDTO> GetGame(long gameID)
        {
            try
            {
                var response = context.Games.Where(x => x.ID == gameID).Select(x => new GameDTO()
                {
                    ID = x.ID,
                    Name = x.Name,
                    MinPlayers = x.MinPlayers,
                    MaxPlayers = x.MaxPlayers
                }
                ).FirstOrDefault();
                return new BaseServiceResponse<GameDTO>(true) { Response = response };
            }
            catch (Exception ex)
            {
                return new BaseServiceResponse<GameDTO>(false) { Message = ex.ToString() };
            }
        }

        public BaseServiceResponse<bool> SaveGame(Game model)
        {
            try
            {
                context.Games.Add(model);
                context.SaveChanges();

                return new BaseServiceResponse<bool>(true) { Response = true };
            }
            catch (Exception ex)
            {
                return new BaseServiceResponse<bool>(false) { Message = ex.ToString() };
            }
        }
    }
}
