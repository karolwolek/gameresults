﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GamesResults.Services
{
    public class BaseServiceResponse<T>
    {

        public BaseServiceResponse()
            : this(true)
        {
        }

        public BaseServiceResponse(bool success)
        {
            this.Success = success;
        }

        public BaseServiceResponse(Exception exception)
            : this(false)
        {
            this.Message = exception.ToString();
        }

        public bool Success { get; private set; }

        public string Message { get; set; }

        public T Response { get; set; }

    }
}
