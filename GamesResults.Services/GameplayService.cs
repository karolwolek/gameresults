﻿using System;
using System.Linq;
using System.Collections.Generic;
using GamesResults.DTO;
using GamesResults.Models;

namespace GamesResults.Services
{
    public class GameplayService
    {
        private GameResultsContext context { get; }

        public GameplayService(GameResultsContext _context)
        {
            context = _context;
        }


        public BaseServiceResponse<List<GameplayDTO>> GetGameplaysList()
        {
            try
            {
                var response = context.Gameplays.Select(x => new GameplayDTO()
                {
                    ID = x.ID,
                    Date = x.Date,
                    GameID = x.GameID,
                    GameName = x.Game.Name
                }
                );
                return new BaseServiceResponse<List<GameplayDTO>>(true) { Response = response.ToList() };
            }
            catch (Exception ex)
            {
                return new BaseServiceResponse<List<GameplayDTO>>(false) { Message = ex.ToString() };
            }
        }


        public BaseServiceResponse<GameplayDTO> GetGameplay(long gameplayID)
        {
            try
            {
                var response = context.Gameplays.Where(x => x.ID == gameplayID).Select(x => new GameplayDTO()
                {
                    ID = x.ID,
                    Date = x.Date,
                    GameID = x.GameID,
                    GameName = x.Game.Name
                }
                ).FirstOrDefault();

                response.PlayersResults = context.PlayerResults.Where(x => x.GameplayID == gameplayID).Select(x => new PlayerResultDTO()
                {
                    ID = x.ID,
                    PlayerID = x.PlayerID,
                    PlayerName = x.Player.Name,
                    IsWinner = x.IsWinner,
                    Result = x.Result
                }).ToList();

                return new BaseServiceResponse<GameplayDTO>(true) { Response = response };
            }
            catch (Exception ex)
            {
                return new BaseServiceResponse<GameplayDTO>(false) { Message = ex.ToString() };
            }
        }


        public BaseServiceResponse<bool> SaveGameplay(Gameplay model)
        {
            try
            {
                context.Gameplays.Add(model);
                context.SaveChanges();

                return new BaseServiceResponse<bool>(true) { Response = true };
            }
            catch (Exception ex)
            {
                return new BaseServiceResponse<bool>(false) { Message = ex.ToString() };
            }
        }

    }
}
