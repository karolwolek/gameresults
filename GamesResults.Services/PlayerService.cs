﻿using System;
using System.Linq;
using System.Collections.Generic;
using GamesResults.DTO;
using GamesResults.Models;

namespace GamesResults.Services
{
    public class PlayerService
    {
        private GameResultsContext context { get; }

        public PlayerService(GameResultsContext _context)
        {
            context = _context;
        }


        public BaseServiceResponse<List<PlayerDTO>> GetPlayersList()
        {
            try
            {
                var response = context.Players.Select(x => new PlayerDTO()
                {
                    ID = x.ID,
                    Name = x.Name
                }
                );
                return new BaseServiceResponse<List<PlayerDTO>>(true) { Response = response.ToList() };
            }
            catch (Exception ex)
            {
                return new BaseServiceResponse<List<PlayerDTO>>(false) { Message = ex.ToString() };
            }
        }

        public BaseServiceResponse<PlayerDTO> GetPlayer(long playerID)
        {
            try
            {
                var response = context.Players.Where(x => x.ID == playerID).Select(x => new PlayerDTO()
                {
                    ID = x.ID,
                    Name = x.Name
                }
                ).FirstOrDefault();
                return new BaseServiceResponse<PlayerDTO>(true) { Response = response };
            }
            catch (Exception ex)
            {
                return new BaseServiceResponse<PlayerDTO>(false) { Message = ex.ToString() };
            }
        }

        public BaseServiceResponse<bool> SavePlayer(Player model)
        {
            try
            {
                context.Players.Add(model);
                context.SaveChanges();

                return new BaseServiceResponse<bool>(true) { Response = true };
            }
            catch (Exception ex)
            {
                return new BaseServiceResponse<bool>(false) { Message = ex.ToString() };
            }
        }

    }
}
