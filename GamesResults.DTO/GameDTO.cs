﻿using System;

namespace GamesResults.DTO
{
    public class GameDTO
    {
        public long ID { get; set; }
        public string Name { get; set; }
        public int MinPlayers { get; set; }
        public int? MaxPlayers { get; set; }
    }
}
