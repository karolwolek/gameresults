﻿using System;

namespace GamesResults.DTO
{
    public class PlayerDTO
    {
        public long ID { get; set; }
        public string Name { get; set; }
    }
}
