﻿using System;
using System.Collections.Generic;

namespace GamesResults.DTO
{
    public class GameplayDTO
    {
        public long ID { get; set; }
        public long GameID { get; set; }
        public DateTime Date { get; set; }
        public string GameName { get; set; }

        public List<PlayerResultDTO> PlayersResults { get; set; }
    }
}
