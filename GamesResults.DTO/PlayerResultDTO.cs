﻿using System;

namespace GamesResults.DTO
{
    public class PlayerResultDTO
    {
        public long ID { get; set; }
        public string PlayerName { get; set; }
        public long PlayerID { get; set; }
        public long Result { get; set; }
        public bool IsWinner { get; set; }
    }
}
