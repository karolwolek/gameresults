﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Configuration;
using Microsoft.Extensions.Configuration;
using System.IO;
using Npgsql;
using Npgsql.Logging;
using Npgsql.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Windows;

namespace GameResults.DBVM
{
    class Program
    {
        public static IConfiguration Configuration { get; set; }

        static void Main(string[] args)
        {
           // var builder = new ConfigurationBuilder()
           //.SetBasePath(Directory.GetCurrentDirectory())
           //.AddJsonFile("appsettings.json");

           // Configuration = builder.Build();

            var connectionString = "Server=localhost;Database=GameResults;User Id=postgres;Password=123456;";
            using (var conn = new NpgsqlConnection(connectionString))
            {
                conn.Open();
                using (var cmd = conn.CreateCommand())
                {
                    try
                    {
                        cmd.CommandText = @"
CREATE TABLE IF NOT EXISTS ""VersionInfo""
(
  ""ID"" bigserial not null,
  ""ScriptName"" text not null
);
";
                        cmd.ExecuteNonQuery();

                        cmd.CommandText = @"SELECT ""ScriptName"" FROM ""VersionInfo""";
                        var processedScripts = new List<string>();
                        using (var reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                var scriptName = reader.GetValue(0) as string;
                                processedScripts.Add(scriptName);
                            }
                        }

                        var dirName = "Scripts";
                        var scriptsDirectory = new DirectoryInfo(dirName);
                        var scriptsToProcess = scriptsDirectory.GetFiles().Where(x => !processedScripts.Contains(x.Name)).Select(x => x.Name);

                        foreach(var script in scriptsToProcess)
                        {
                            var scriptText = File.OpenText(string.Format("{0}/{1}",dirName,script)).ReadToEnd();
                            cmd.CommandText = scriptText + AddScript(script);
                            cmd.ExecuteNonQuery();
                            Console.WriteLine(string.Format("Migracja skryptu {0} przebiegła pomyślnie.",script));
                        }
                    }
                    catch(Exception ex)
                    {
                        Console.WriteLine(ex.ToString());
                    }
                }
            }
        

        }

        static string AddScript(string script)
        {
            return string.Format(@"INSERT INTO ""VersionInfo"" (""ScriptName"") VALUES ('{0}');",script);
        }
    }
}
