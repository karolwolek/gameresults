﻿CREATE TABLE "Gameplay"
(
"ID" bigserial NOT NULL,
"Date" timestamp NOT NULL,
"GameID" bigint NOT NULL,
CONSTRAINT "PK_Gameplay" PRIMARY KEY ("ID"),
CONSTRAINT "FK_Gameplay_GameID" FOREIGN KEY ("GameID") REFERENCES "Game" ("ID")

);