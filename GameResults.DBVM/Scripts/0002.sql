﻿CREATE TABLE "Game"
(
"ID" bigserial NOT NULL,
"Name" text not null,
"MinPlayers" int NOT NULL,
"MaxPlayers" int,
  CONSTRAINT "PK_Game" PRIMARY KEY ("ID")
);