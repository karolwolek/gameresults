﻿CREATE TABLE "PlayerResult"
(
"ID" bigserial NOT NULL,
"Result" int,
"IsWinner" boolean,
"GameplayID" bigint NOT NULL,
"PlayerID" bigint NOT NULL,
CONSTRAINT "PK_PlayerResult" PRIMARY KEY ("ID"),
CONSTRAINT "FK_PlayerResult_GameplayID" FOREIGN KEY ("GameplayID") REFERENCES "Gameplay" ("ID"),
CONSTRAINT "FK_PlayerResult_PlayerID" FOREIGN KEY ("PlayerID") REFERENCES "Player" ("ID")

);